package springboot.entity;

import lombok.Data;

/**
 * @ClassnameXcResultEntity
 * @Description TODO
 * @Date 2021/12/25 11:52
 * @Created by GS
 **/
@Data
public class XcResultEntity {
    long allxc_sss;
    long allxc_ss;
    long allxc_s;
    long allxc_a;
    long invxc1_per_sss;
    long invxc1_per_ss;
    long invxc1_per_s;
    long invxc1_per_a;
    long invxc1_ent_sss;
    long invxc1_ent_ss;
    long invxc1_ent_s;
    long invxc1_ent_a;

    long invxc2_per_sss;
    long invxc2_per_ss;
    long invxc2_per_s;
    long invxc2_per_a;
    long invxc2_ent_sss;
    long invxc2_ent_ss;
    long invxc2_ent_s;
    long invxc2_ent_a;

    long invxc3_per_sss;
    long invxc3_per_ss;
    long invxc3_per_s;
    long invxc3_per_a;
    long invxc3_ent_sss;
    long invxc3_ent_ss;
    long invxc3_ent_s;
    long invxc3_ent_a;

    long invxc4_per_sss;
    long invxc4_per_ss;
    long invxc4_per_s;
    long invxc4_per_a;
    long invxc4_ent_sss;
    long invxc4_ent_ss;
    long invxc4_ent_s;
    long invxc4_ent_a;

    long invxc5_per_sss;
    long invxc5_per_ss;
    long invxc5_per_s;
    long invxc5_per_a;
    long invxc5_ent_sss;
    long invxc5_ent_ss;
    long invxc5_ent_s;
    long invxc5_ent_a;

    long invxc6_per_sss;
    long invxc6_per_ss;
    long invxc6_per_s;
    long invxc6_per_a;
    long invxc6_ent_sss;
    long invxc6_ent_ss;
    long invxc6_ent_s;
    long invxc6_ent_a;

    long invxc7_per_sss;
    long invxc7_per_ss;
    long invxc7_per_s;
    long invxc7_per_a;
    long invxc7_ent_sss;
    long invxc7_ent_ss;
    long invxc7_ent_s;
    long invxc7_ent_a;

    long invxc8_per_sss;
    long invxc8_per_ss;
    long invxc8_per_s;
    long invxc8_per_a;
    long invxc8_ent_sss;
    long invxc8_ent_ss;
    long invxc8_ent_s;
    long invxc8_ent_a;

    long invxc9_per_sss;
    long invxc9_per_ss;
    long invxc9_per_s;
    long invxc9_per_a;
    long invxc9_ent_sss;
    long invxc9_ent_ss;
    long invxc9_ent_s;
    long invxc9_ent_a;

    long invxc10_per_sss;
    long invxc10_per_ss;
    long invxc10_per_s;
    long invxc10_per_a;
    long invxc10_ent_sss;
    long invxc10_ent_ss;
    long invxc10_ent_s;
    long invxc10_ent_a;

    long invxc11_per_sss;
    long invxc11_per_ss;
    long invxc11_per_s;
    long invxc11_per_a;
    long invxc11_ent_sss;
    long invxc11_ent_ss;
    long invxc11_ent_s;
    long invxc11_ent_a;

    long invxc12_per_sss;
    long invxc12_per_ss;
    long invxc12_per_s;
    long invxc12_per_a;
    long invxc12_ent_sss;
    long invxc12_ent_ss;
    long invxc12_ent_s;
    long invxc12_ent_a;

    long invxc13_per_sss;
    long invxc13_per_ss;
    long invxc13_per_s;
    long invxc13_per_a;
    long invxc13_ent_sss;
    long invxc13_ent_ss;
    long invxc13_ent_s;
    long invxc13_ent_a;

    long invxc14_per_sss;
    long invxc14_per_ss;
    long invxc14_per_s;
    long invxc14_per_a;
    long invxc14_ent_sss;
    long invxc14_ent_ss;
    long invxc14_ent_s;
    long invxc14_ent_a;

    long invxc15_per_sss;
    long invxc15_per_ss;
    long invxc15_per_s;
    long invxc15_per_a;
    long invxc15_ent_sss;
    long invxc15_ent_ss;
    long invxc15_ent_s;
    long invxc15_ent_a;

    long invxc16_per_sss;
    long invxc16_per_ss;
    long invxc16_per_s;
    long invxc16_per_a;
    long invxc16_ent_sss;
    long invxc16_ent_ss;
    long invxc16_ent_s;
    long invxc16_ent_a;

    long invxc17_per_sss;
    long invxc17_per_ss;
    long invxc17_per_s;
    long invxc17_per_a;
    long invxc17_ent_sss;
    long invxc17_ent_ss;
    long invxc17_ent_s;
    long invxc17_ent_a;

    long invxc18_per_sss;
    long invxc18_per_ss;
    long invxc18_per_s;
    long invxc18_per_a;
    long invxc18_ent_sss;
    long invxc18_ent_ss;
    long invxc18_ent_s;
    long invxc18_ent_a;

    long invxc19_per_sss;
    long invxc19_per_ss;
    long invxc19_per_s;
    long invxc19_per_a;
    long invxc19_ent_sss;
    long invxc19_ent_ss;
    long invxc19_ent_s;
    long invxc19_ent_a;

    long invxc20_per_sss;
    long invxc20_per_ss;
    long invxc20_per_s;
    long invxc20_per_a;
    long invxc20_ent_sss;
    long invxc20_ent_ss;
    long invxc20_ent_s;
    long invxc20_ent_a;

    long invxc22_per_sss;
    long invxc22_per_ss;
    long invxc22_per_s;
    long invxc22_per_a;
    long invxc22_ent_sss;
    long invxc22_ent_ss;
    long invxc22_ent_s;
    long invxc22_ent_a;



}
