package springboot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

/**
 * @ClassnameXcInsertEntity
 * @Description TODO
 * @Date 2021/12/25 11:48
 * @Created by GS
 **/
@TableName("tb_xcinv") //和数据库中的表对应
@AllArgsConstructor
@NoArgsConstructor
@Data
public class XcInsertEntity {
    @TableId(value = "id", type = IdType.AUTO)
    Integer id;
    int unid;             //被评价单位ID
    String allxc;          //总体评价
    String objxc;              //评价人身份：企业人员或个人
    String invxc1;             //第一项评价内容
    String invxc2;            //第二项评价内容
    String invxc3;
    String invxc4;
    String invxc5;
    String invxc6;
    String invxc7;
    String invxc8;
    String invxc9;
    String invxc10;
    String invxc11;
    String invxc12;
    String invxc13;
    String invxc14;
    String invxc15;
    String invxc16;
    String invxc17;
    String invxc18;
    String invxc19;
    String invxc20;
    Date   invxc21;
    String xcopi;
    String invxc22;//评价建议
}


