package springboot.entity;

import lombok.Data;

@Data
public class GajEntity {
    long gajall_sss;
    long gajall_ss;
    long gajall_s;
    long gajall_a;
    long gajinv1_per_sss;
    long gajinv1_per_ss;
    long gajinv1_per_s;
    long gajinv1_per_a;
    long gajinv1_ent_sss;
    long gajinv1_ent_ss;
    long gajinv1_ent_s;
    long gajinv1_ent_a;

    long gajinv2_per_sss;
    long gajinv2_per_ss;
    long gajinv2_per_s;
    long gajinv2_per_a;
    long gajinv2_ent_sss;
    long gajinv2_ent_ss;
    long gajinv2_ent_s;
    long gajinv2_ent_a;

    long gajinv3_per_sss;
    long gajinv3_per_ss;
    long gajinv3_per_s;
    long gajinv3_per_a;
    long gajinv3_ent_sss;
    long gajinv3_ent_ss;
    long gajinv3_ent_s;
    long gajinv3_ent_a;

    long gajinv4_per_sss;
    long gajinv4_per_ss;
    long gajinv4_per_s;
    long gajinv4_per_a;
    long gajinv4_ent_sss;
    long gajinv4_ent_ss;
    long gajinv4_ent_s;
    long gajinv4_ent_a;

    long gajinv5_per_sss;
    long gajinv5_per_ss;
    long gajinv5_per_s;
    long gajinv5_per_a;
    long gajinv5_ent_sss;
    long gajinv5_ent_ss;
    long gajinv5_ent_s;
    long gajinv5_ent_a;

    long gajinv6_per_sss;
    long gajinv6_per_ss;
    long gajinv6_per_s;
    long gajinv6_per_a;
    long gajinv6_ent_sss;
    long gajinv6_ent_ss;
    long gajinv6_ent_s;
    long gajinv6_ent_a;

}
