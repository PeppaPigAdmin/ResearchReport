package springboot.entity;

import lombok.Data;

/**
 * @Classname ResultEntity 市本级单位结果集实体类
 * @Description TODO
 * @Date 2021/12/8 15:05
 * @Created by GS
 */
@Data
public class ResultEntity {
    long zjjall_sss;         //单位总体评价非常满意数
    long zjjall_ss;          //单位总体评价比较满意数
    long zjjall_s;           //总体评价基本满意数
    long zjjall_a;           //总体评价不满意数
    long zjjinv1_per_sss;   //inv1代表服务事项1，per代表个人，sss代表非常满意
    long zjjinv1_per_ss;    //ss 代表比较满意
    long zjjinv1_per_s;     //s 代表基本满意
    long zjjinv1_per_a;
    long zjjinv1_ent_sss;
    long zjjinv1_ent_ss;
    long zjjinv1_ent_s;
    long zjjinv1_ent_a;

    long zjjinv2_per_sss;   //inv1代表服务事项1，per代表个人，sss代表非常满意
    long zjjinv2_per_ss;    //ss 代表比较满意
    long zjjinv2_per_s;     //s 代表基本满意
    long zjjinv2_per_a;     //a 代表不满意
    long zjjinv2_ent_sss;   //
    long zjjinv2_ent_ss;
    long zjjinv2_ent_s;
    long zjjinv2_ent_a;

    long zjjinv3_per_sss;
    long zjjinv3_per_ss;
    long zjjinv3_per_s;
    long zjjinv3_per_a;
    long zjjinv3_ent_sss;
    long zjjinv3_ent_ss;
    long zjjinv3_ent_s;
    long zjjinv3_ent_a;

    long zjjinv4_per_sss;
    long zjjinv4_per_ss;
    long zjjinv4_per_s;
    long zjjinv4_per_a;
    long zjjinv4_ent_sss;
    long zjjinv4_ent_ss;
    long zjjinv4_ent_s;
    long zjjinv4_ent_a;

    long zjjinv5_per_sss;
    long zjjinv5_per_ss;
    long zjjinv5_per_s;
    long zjjinv5_per_a;
    long zjjinv5_ent_sss;
    long zjjinv5_ent_ss;
    long zjjinv5_ent_s;
    long zjjinv5_ent_a;

    long zjjinv6_per_sss;
    long zjjinv6_per_ss;
    long zjjinv6_per_s;
    long zjjinv6_per_a;
    long zjjinv6_ent_sss;
    long zjjinv6_ent_ss;
    long zjjinv6_ent_s;
    long zjjinv6_ent_a;
    long zjjinv7_per_sss;
    long zjjinv7_per_ss;
    long zjjinv7_per_s;
    long zjjinv7_per_a;
    long zjjinv7_ent_sss;
    long zjjinv7_ent_ss;
    long zjjinv7_ent_s;
    long zjjinv7_ent_a;
    long zjjinv8_per_sss;
    long zjjinv8_per_ss;
    long zjjinv8_per_s;
    long zjjinv8_per_a;
    long zjjinv8_ent_sss;
    long zjjinv8_ent_ss;
    long zjjinv8_ent_s;
    long zjjinv8_ent_a;

    long zjjinv9_per_sss;
    long zjjinv9_per_ss;
    long zjjinv9_per_s;
    long zjjinv9_per_a;
    long zjjinv9_ent_sss;
    long zjjinv9_ent_ss;
    long zjjinv9_ent_s;
    long zjjinv9_ent_a;

    long zjjinv10_per_sss;
    long zjjinv10_per_ss;
    long zjjinv10_per_s;
    long zjjinv10_per_a;
    long zjjinv10_ent_sss;
    long zjjinv10_ent_ss;
    long zjjinv10_ent_s;
    long zjjinv10_ent_a;

    long zjjinv11_per_sss;
    long zjjinv11_per_ss;
    long zjjinv11_per_s;
    long zjjinv11_per_a;
    long zjjinv11_ent_sss;
    long zjjinv11_ent_ss;
    long zjjinv11_ent_s;
    long zjjinv11_ent_a;

    long zjjinv12_per_sss;
    long zjjinv12_per_ss;
    long zjjinv12_per_s;
    long zjjinv12_per_a;
    long zjjinv12_ent_sss;
    long zjjinv12_ent_ss;
    long zjjinv12_ent_s;
    long zjjinv12_ent_a;

    long zjjinv13_per_sss;
    long zjjinv13_per_ss;
    long zjjinv13_per_s;
    long zjjinv13_per_a;
    long zjjinv13_ent_sss;
    long zjjinv13_ent_ss;
    long zjjinv13_ent_s;
    long zjjinv13_ent_a;


}
