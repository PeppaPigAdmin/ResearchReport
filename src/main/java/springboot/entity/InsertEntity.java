package springboot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

/**
 * 市直单位评价表
 *
 * @Classname PeopleResourcesEntity
 * @Description TODO
 * @Date 2021/11/29 11:14
 * @Created by GS
 */
@TableName("tb_inv") //和数据库中的表对应
@AllArgsConstructor
@NoArgsConstructor
@Data
public class InsertEntity {
    @TableId(value = "id", type = IdType.AUTO)
    Integer id;
    int unit_id;             //被评价单位ID
    String all_inv;          //总体评价
    String obj;              //评价人身份：企业人员或个人
    String inv1;             //第一项评价内容
    String inv2;            //第二项评价内容
    String inv3;
    String inv4;
    String inv5;
    String inv6;
    String inv7;
    String inv8;
    String inv9;
    String inv10;
    String inv11;
    String inv12;
    Date inv13;
    String opi;              //评价建议
}

