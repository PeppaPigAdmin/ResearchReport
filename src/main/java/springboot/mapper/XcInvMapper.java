package springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import springboot.entity.XcInsertEntity;

/**
 * @ClassnameXcInvMapper
 * @Description TODO
 * @Date 2021/12/25 12:23
 * @Created by GS
 **/

@Repository //在spring框架中进行注册
public interface XcInvMapper extends BaseMapper<XcInsertEntity> {

}


