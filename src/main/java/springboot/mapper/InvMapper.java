package springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import springboot.entity.InsertEntity;

/**
 * @Classname InvMapper
 * @Description TODO
 * @Date 2021/12/1 13:24
 * @Created by GS
 */
@Repository //在spring框架中进行注册
public interface InvMapper extends BaseMapper<InsertEntity> {

}
