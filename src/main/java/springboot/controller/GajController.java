package springboot.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springboot.entity.GajEntity;
import springboot.entity.InsertEntity;
import springboot.mapper.InvMapper;

@RestController
@RequestMapping("GajController")
public class GajController {
    @Autowired
    InvMapper invMapper;

    //公安局查询数据
    @RequestMapping("/result")
    public String getGajAll(){
        try {
            GajEntity gajEntity = new GajEntity();
            QueryWrapper<InsertEntity> queryWrapper = new QueryWrapper<>();
            long all_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("all_inv", "非常满意"));
            queryWrapper.clear();
            long all_good = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("all_inv", "比较满意"));
            queryWrapper.clear();
            long all_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("all_inv", "基本满意"));
            queryWrapper.clear();
            long all_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("all_inv", "不满意"));
            queryWrapper.clear();

            long inv1_personal_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv1", "非常满意"));
            queryWrapper.clear();
            long inv1_personal_good = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv1", "比较满意"));
            queryWrapper.clear();
            long inv1_personal_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv1", "基本满意"));
            queryWrapper.clear();
            long inv1_personal_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv1", "不满意"));
            queryWrapper.clear();
            long inv1_enterprise_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv1", "非常满意"));
            queryWrapper.clear();
            long inv1_enterprise_good = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv1", "比较满意"));
            queryWrapper.clear();
            long inv1_enterprise_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv1", "基本满意"));
            queryWrapper.clear();
            long inv1_enterprise_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv1", "不满意"));
            queryWrapper.clear();

            long inv2_personal_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv2", "非常满意"));
            queryWrapper.clear();
            long inv2_personal_good = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv2", "比较满意"));
            queryWrapper.clear();
            long inv2_personal_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv2", "基本满意"));
            queryWrapper.clear();
            long inv2_personal_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv2", "不满意"));
            queryWrapper.clear();
            long inv2_enterprise_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv2", "非常满意"));
            queryWrapper.clear();
            long inv2_enterprise_good = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv2", "比较满意"));
            queryWrapper.clear();
            long inv2_enterprise_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv2", "基本满意"));
            queryWrapper.clear();
            long inv2_enterprise_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv2", "不满意"));
            queryWrapper.clear();

            long inv3_personal_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv3", "非常满意"));
            queryWrapper.clear();
            long inv3_personal_good = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv3", "比较满意"));
            queryWrapper.clear();
            long inv3_personal_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv3", "基本满意"));
            queryWrapper.clear();
            long inv3_personal_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv3", "不满意"));
            queryWrapper.clear();
            long inv3_enterprise_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv3", "非常满意"));
            queryWrapper.clear();
            long inv3_enterprise_good = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv3", "比较满意"));
            queryWrapper.clear();
            long inv3_enterprise_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv3", "基本满意"));
            queryWrapper.clear();
            long inv3_enterprise_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv3", "不满意"));
            queryWrapper.clear();

            long inv4_personal_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv4", "非常满意"));
            queryWrapper.clear();
            long inv4_personal_good = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv4", "比较满意"));
            queryWrapper.clear();
            long inv4_personal_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv4", "基本满意"));
            queryWrapper.clear();
            long inv4_personal_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv4", "不满意"));
            queryWrapper.clear();
            long inv4_enterprise_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv4", "非常满意"));
            queryWrapper.clear();
            long inv4_enterprise_good = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv4", "比较满意"));
            queryWrapper.clear();
            long inv4_enterprise_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv4", "基本满意"));
            queryWrapper.clear();
            long inv4_enterprise_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv4", "不满意"));
            queryWrapper.clear();

            long inv5_personal_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv5", "非常满意"));
            queryWrapper.clear();
            long inv5_personal_good = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv5", "比较满意"));
            queryWrapper.clear();
            long inv5_personal_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv5", "基本满意"));
            queryWrapper.clear();
            long inv5_personal_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv5", "不满意"));
            queryWrapper.clear();
            long inv5_enterprise_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv5", "非常满意"));
            queryWrapper.clear();
            long inv5_enterprise_good = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv5", "比较满意"));
            queryWrapper.clear();
            long inv5_enterprise_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv5", "基本满意"));
            queryWrapper.clear();
            long inv5_enterprise_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv5", "不满意"));
            queryWrapper.clear();

            long inv6_personal_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv6", "非常满意"));
            queryWrapper.clear();
            long inv6_personal_good = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv6", "比较满意"));
            queryWrapper.clear();
            long inv6_personal_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv6", "基本满意"));
            queryWrapper.clear();
            long inv6_personal_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "个人").eq("inv6", "不满意"));
            queryWrapper.clear();
            long inv6_enterprise_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv6", "非常满意"));
            queryWrapper.clear();
            long inv6_enterprise_good = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv6", "比较满意"));
            queryWrapper.clear();
            long inv6_enterprise_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv6", "基本满意"));
            queryWrapper.clear();
            long inv6_enterprise_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "3").eq("obj", "企业").eq("inv6", "不满意"));
            queryWrapper.clear();

            gajEntity.setGajinv6_per_sss(inv6_personal_verygood);
            gajEntity.setGajinv6_per_ss(inv6_personal_good);
            gajEntity.setGajinv6_per_s(inv6_personal_satisfied);
            gajEntity.setGajinv6_per_a(inv6_personal_dissatisfied);
            gajEntity.setGajinv6_ent_sss(inv6_enterprise_verygood);
            gajEntity.setGajinv6_ent_ss(inv6_enterprise_good);
            gajEntity.setGajinv6_ent_s(inv6_enterprise_satisfied);
            gajEntity.setGajinv6_ent_a(inv6_enterprise_dissatisfied);

            gajEntity.setGajinv5_per_sss(inv5_personal_verygood);
            gajEntity.setGajinv5_per_ss(inv5_personal_good);
            gajEntity.setGajinv5_per_s(inv5_personal_satisfied);
            gajEntity.setGajinv5_per_a(inv5_personal_dissatisfied);
            gajEntity.setGajinv5_ent_sss(inv5_enterprise_verygood);
            gajEntity.setGajinv5_ent_ss(inv5_enterprise_good);
            gajEntity.setGajinv5_ent_s(inv5_enterprise_satisfied);
            gajEntity.setGajinv5_ent_a(inv5_enterprise_dissatisfied);

            gajEntity.setGajinv4_per_sss(inv4_personal_verygood);
            gajEntity.setGajinv4_per_ss(inv4_personal_good);
            gajEntity.setGajinv4_per_s(inv4_personal_satisfied);
            gajEntity.setGajinv4_per_a(inv4_personal_dissatisfied);
            gajEntity.setGajinv4_ent_sss(inv4_enterprise_verygood);
            gajEntity.setGajinv4_ent_ss(inv4_enterprise_good);
            gajEntity.setGajinv4_ent_s(inv4_enterprise_satisfied);
            gajEntity.setGajinv4_ent_a(inv4_enterprise_dissatisfied);

            gajEntity.setGajinv3_per_sss(inv3_personal_verygood);
            gajEntity.setGajinv3_per_ss(inv3_personal_good);
            gajEntity.setGajinv3_per_s(inv3_personal_satisfied);
            gajEntity.setGajinv3_per_a(inv3_personal_dissatisfied);
            gajEntity.setGajinv3_ent_sss(inv3_enterprise_verygood);
            gajEntity.setGajinv3_ent_ss(inv3_enterprise_good);
            gajEntity.setGajinv3_ent_s(inv3_enterprise_satisfied);
            gajEntity.setGajinv3_ent_a(inv3_enterprise_dissatisfied);

            gajEntity.setGajinv2_per_sss(inv2_personal_verygood);
            gajEntity.setGajinv2_per_ss(inv2_personal_good);
            gajEntity.setGajinv2_per_s(inv2_personal_satisfied);
            gajEntity.setGajinv2_per_a(inv2_personal_dissatisfied);
            gajEntity.setGajinv2_ent_sss(inv2_enterprise_verygood);
            gajEntity.setGajinv2_ent_ss(inv2_enterprise_good);
            gajEntity.setGajinv2_ent_s(inv2_enterprise_satisfied);
            gajEntity.setGajinv2_ent_a(inv2_enterprise_dissatisfied);

            gajEntity.setGajinv1_per_sss(inv1_personal_verygood);
            gajEntity.setGajinv1_per_ss(inv1_personal_good);
            gajEntity.setGajinv1_per_s(inv1_personal_satisfied);
            gajEntity.setGajinv1_per_a(inv1_personal_dissatisfied);
            gajEntity.setGajinv1_ent_sss(inv1_enterprise_verygood);
            gajEntity.setGajinv1_ent_ss(inv1_enterprise_good);
            gajEntity.setGajinv1_ent_s(inv1_enterprise_satisfied);
            gajEntity.setGajinv1_ent_a(inv1_enterprise_dissatisfied);

            gajEntity.setGajall_sss(all_verygood);
            gajEntity.setGajall_ss(all_good);
            gajEntity.setGajall_s(all_satisfied);
            gajEntity.setGajall_a(all_dissatisfied);
            JSONObject jso = JSONObject.fromObject(gajEntity);

            return jso.toString();

        }catch (Exception e ){
            e.printStackTrace();
            return null;
        }
    };
}
