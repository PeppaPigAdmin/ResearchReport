package springboot.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springboot.entity.InsertEntity;
import springboot.entity.ResultEntity;
import springboot.mapper.InvMapper;

/**
 * @ClassnameNyncjController
 * @Description TODO
 * @Date 2022/1/2 15:38
 * @Created by GS
 **/
@RestController
@RequestMapping("NyncjController")
public class NyncjController {
    @Autowired
    InvMapper invMapper;
    @RequestMapping("/result")
    public String getNyncjAll(){
        try {
            ResultEntity result = new ResultEntity();
            QueryWrapper<InsertEntity> queryWrapper = new QueryWrapper<>();
            long all_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("all_inv", "非常满意"));
            queryWrapper.clear();
            long all_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("all_inv", "比较满意"));
            queryWrapper.clear();
            long all_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("all_inv", "基本满意"));
            queryWrapper.clear();
            long all_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("all_inv", "不满意"));
            queryWrapper.clear();

            long inv1_personal_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv1", "非常满意"));
            queryWrapper.clear();
            long inv1_personal_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv1", "比较满意"));
            queryWrapper.clear();
            long inv1_personal_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv1", "基本满意"));
            queryWrapper.clear();
            long inv1_personal_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv1", "不满意"));
            queryWrapper.clear();
            long inv1_enterprise_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv1", "非常满意"));
            queryWrapper.clear();
            long inv1_enterprise_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv1", "比较满意"));
            queryWrapper.clear();
            long inv1_enterprise_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv1", "基本满意"));
            queryWrapper.clear();
            long inv1_enterprise_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv1", "不满意"));
            queryWrapper.clear();

            long inv2_personal_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv2", "非常满意"));
            queryWrapper.clear();
            long inv2_personal_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv2", "比较满意"));
            queryWrapper.clear();
            long inv2_personal_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv2", "基本满意"));
            queryWrapper.clear();
            long inv2_personal_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv2", "不满意"));
            queryWrapper.clear();
            long inv2_enterprise_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv2", "非常满意"));
            queryWrapper.clear();
            long inv2_enterprise_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv2", "比较满意"));
            queryWrapper.clear();
            long inv2_enterprise_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv2", "基本满意"));
            queryWrapper.clear();
            long inv2_enterprise_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv2", "不满意"));
            queryWrapper.clear();

            long inv3_personal_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv3", "非常满意"));
            queryWrapper.clear();
            long inv3_personal_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv3", "比较满意"));
            queryWrapper.clear();
            long inv3_personal_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv3", "基本满意"));
            queryWrapper.clear();
            long inv3_personal_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv3", "不满意"));
            queryWrapper.clear();
            long inv3_enterprise_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv3", "非常满意"));
            queryWrapper.clear();
            long inv3_enterprise_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv3", "比较满意"));
            queryWrapper.clear();
            long inv3_enterprise_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv3", "基本满意"));
            queryWrapper.clear();
            long inv3_enterprise_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv3", "不满意"));
            queryWrapper.clear();

            long inv4_personal_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv4", "非常满意"));
            queryWrapper.clear();
            long inv4_personal_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv4", "比较满意"));
            queryWrapper.clear();
            long inv4_personal_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv4", "基本满意"));
            queryWrapper.clear();
            long inv4_personal_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv4", "不满意"));
            queryWrapper.clear();
            long inv4_enterprise_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv4", "非常满意"));
            queryWrapper.clear();
            long inv4_enterprise_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv4", "比较满意"));
            queryWrapper.clear();
            long inv4_enterprise_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv4", "基本满意"));
            queryWrapper.clear();
            long inv4_enterprise_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv4", "不满意"));
            queryWrapper.clear();

            long inv5_personal_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv5", "非常满意"));
            queryWrapper.clear();
            long inv5_personal_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv5", "比较满意"));
            queryWrapper.clear();
            long inv5_personal_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv5", "基本满意"));
            queryWrapper.clear();
            long inv5_personal_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv5", "不满意"));
            queryWrapper.clear();
            long inv5_enterprise_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv5", "非常满意"));
            queryWrapper.clear();
            long inv5_enterprise_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv5", "比较满意"));
            queryWrapper.clear();
            long inv5_enterprise_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv5", "基本满意"));
            queryWrapper.clear();
            long inv5_enterprise_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv5", "不满意"));
            queryWrapper.clear();

            long inv6_personal_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv6", "非常满意"));
            queryWrapper.clear();
            long inv6_personal_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv6", "比较满意"));
            queryWrapper.clear();
            long inv6_personal_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv6", "基本满意"));
            queryWrapper.clear();
            long inv6_personal_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv6", "不满意"));
            queryWrapper.clear();
            long inv6_enterprise_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv6", "非常满意"));
            queryWrapper.clear();
            long inv6_enterprise_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv6", "比较满意"));
            queryWrapper.clear();
            long inv6_enterprise_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv6", "基本满意"));
            queryWrapper.clear();
            long inv6_enterprise_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv6", "不满意"));
            queryWrapper.clear();
            long inv7_personal_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv6", "非常满意"));
            queryWrapper.clear();
            long inv7_personal_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv7", "比较满意"));
            queryWrapper.clear();
            long inv7_personal_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv7", "基本满意"));
            queryWrapper.clear();
            long inv7_personal_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv7", "不满意"));
            queryWrapper.clear();
            long inv7_enterprise_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv7", "非常满意"));
            queryWrapper.clear();
            long inv7_enterprise_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv7", "比较满意"));
            queryWrapper.clear();
            long inv7_enterprise_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv7", "基本满意"));
            queryWrapper.clear();
            long inv7_enterprise_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv7", "不满意"));
            queryWrapper.clear();
            long inv8_personal_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv8", "非常满意"));
            queryWrapper.clear();
            long inv8_personal_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv8", "比较满意"));
            queryWrapper.clear();
            long inv8_personal_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv8", "基本满意"));
            queryWrapper.clear();
            long inv8_personal_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv8", "不满意"));
            queryWrapper.clear();
            long inv8_enterprise_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv8", "非常满意"));
            queryWrapper.clear();
            long inv8_enterprise_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv8", "比较满意"));
            queryWrapper.clear();
            long inv8_enterprise_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv8", "基本满意"));
            queryWrapper.clear();
            long inv8_enterprise_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv8", "不满意"));
            queryWrapper.clear();
            long inv9_personal_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv9", "非常满意"));
            queryWrapper.clear();
            long inv9_personal_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv9", "比较满意"));
            queryWrapper.clear();
            long inv9_personal_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv9", "基本满意"));
            queryWrapper.clear();
            long inv9_personal_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv9", "不满意"));
            queryWrapper.clear();
            long inv9_enterprise_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv9", "非常满意"));
            queryWrapper.clear();
            long inv9_enterprise_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv9", "比较满意"));
            queryWrapper.clear();
            long inv9_enterprise_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv9", "基本满意"));
            queryWrapper.clear();
            long inv9_enterprise_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv9", "不满意"));
            queryWrapper.clear();
            long inv10_personal_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv10", "非常满意"));
            queryWrapper.clear();
            long inv10_personal_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv10", "比较满意"));
            queryWrapper.clear();
            long inv10_personal_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv10", "基本满意"));
            queryWrapper.clear();
            long inv10_personal_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv10", "不满意"));
            queryWrapper.clear();
            long inv10_enterprise_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv10", "非常满意"));
            queryWrapper.clear();
            long inv10_enterprise_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv10", "比较满意"));
            queryWrapper.clear();
            long inv10_enterprise_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv10", "基本满意"));
            queryWrapper.clear();
            long inv10_enterprise_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv10", "不满意"));
            queryWrapper.clear();
            long inv11_personal_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv11", "非常满意"));
            queryWrapper.clear();
            long inv11_personal_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv11", "比较满意"));
            queryWrapper.clear();
            long inv11_personal_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv11", "基本满意"));
            queryWrapper.clear();
            long inv11_personal_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "个人").eq("inv11", "不满意"));
            queryWrapper.clear();
            long inv11_enterprise_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv11", "非常满意"));
            queryWrapper.clear();
            long inv11_enterprise_good = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv11", "比较满意"));
            queryWrapper.clear();
            long inv11_enterprise_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv11", "基本满意"));
            queryWrapper.clear();
            long inv11_enterprise_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "8").eq("obj", "企业").eq("inv11", "不满意"));
            queryWrapper.clear();

            result.setZjjinv11_per_sss(inv11_personal_verygood);
            result.setZjjinv11_per_ss(inv11_personal_good);
            result.setZjjinv11_per_s(inv11_personal_satisfied);
            result.setZjjinv11_per_a(inv11_personal_dissatisfied);
            result.setZjjinv11_ent_sss(inv11_enterprise_verygood);
            result.setZjjinv11_ent_ss(inv11_enterprise_good);
            result.setZjjinv11_ent_s(inv11_enterprise_satisfied);
            result.setZjjinv11_ent_a(inv11_enterprise_dissatisfied);

            result.setZjjinv10_per_sss(inv10_personal_verygood);
            result.setZjjinv10_per_ss(inv10_personal_good);
            result.setZjjinv10_per_s(inv10_personal_satisfied);
            result.setZjjinv10_per_a(inv10_personal_dissatisfied);
            result.setZjjinv10_ent_sss(inv10_enterprise_verygood);
            result.setZjjinv10_ent_ss(inv10_enterprise_good);
            result.setZjjinv10_ent_s(inv10_enterprise_satisfied);
            result.setZjjinv10_ent_a(inv10_enterprise_dissatisfied);

            result.setZjjinv9_per_sss(inv9_personal_verygood);
            result.setZjjinv9_per_ss(inv9_personal_good);
            result.setZjjinv9_per_s(inv9_personal_satisfied);
            result.setZjjinv9_per_a(inv9_personal_dissatisfied);
            result.setZjjinv9_ent_sss(inv9_enterprise_verygood);
            result.setZjjinv9_ent_ss(inv9_enterprise_good);
            result.setZjjinv9_ent_s(inv9_enterprise_satisfied);
            result.setZjjinv9_ent_a(inv9_enterprise_dissatisfied);

            result.setZjjinv8_per_sss(inv8_personal_verygood);
            result.setZjjinv8_per_ss(inv8_personal_good);
            result.setZjjinv8_per_s(inv8_personal_satisfied);
            result.setZjjinv8_per_a(inv8_personal_dissatisfied);
            result.setZjjinv8_ent_sss(inv8_enterprise_verygood);
            result.setZjjinv8_ent_ss(inv8_enterprise_good);
            result.setZjjinv8_ent_s(inv8_enterprise_satisfied);
            result.setZjjinv8_ent_a(inv8_enterprise_dissatisfied);

            result.setZjjinv7_per_sss(inv7_personal_verygood);
            result.setZjjinv7_per_ss(inv7_personal_good);
            result.setZjjinv7_per_s(inv7_personal_satisfied);
            result.setZjjinv7_per_a(inv7_personal_dissatisfied);
            result.setZjjinv7_ent_sss(inv7_enterprise_verygood);
            result.setZjjinv7_ent_ss(inv7_enterprise_good);
            result.setZjjinv7_ent_s(inv7_enterprise_satisfied);
            result.setZjjinv7_ent_a(inv7_enterprise_dissatisfied);

            result.setZjjinv6_per_sss(inv6_personal_verygood);
            result.setZjjinv6_per_ss(inv6_personal_good);
            result.setZjjinv6_per_s(inv6_personal_satisfied);
            result.setZjjinv6_per_a(inv6_personal_dissatisfied);
            result.setZjjinv6_ent_sss(inv6_enterprise_verygood);
            result.setZjjinv6_ent_ss(inv6_enterprise_good);
            result.setZjjinv6_ent_s(inv6_enterprise_satisfied);
            result.setZjjinv6_ent_a(inv6_enterprise_dissatisfied);

            result.setZjjinv5_per_sss(inv5_personal_verygood);
            result.setZjjinv5_per_ss(inv5_personal_good);
            result.setZjjinv5_per_s(inv5_personal_satisfied);
            result.setZjjinv5_per_a(inv5_personal_dissatisfied);
            result.setZjjinv5_ent_sss(inv5_enterprise_verygood);
            result.setZjjinv5_ent_ss(inv5_enterprise_good);
            result.setZjjinv5_ent_s(inv5_enterprise_satisfied);
            result.setZjjinv5_ent_a(inv5_enterprise_dissatisfied);

            result.setZjjinv4_per_sss(inv4_personal_verygood);
            result.setZjjinv4_per_ss(inv4_personal_good);
            result.setZjjinv4_per_s(inv4_personal_satisfied);
            result.setZjjinv4_per_a(inv4_personal_dissatisfied);
            result.setZjjinv4_ent_sss(inv4_enterprise_verygood);
            result.setZjjinv4_ent_ss(inv4_enterprise_good);
            result.setZjjinv4_ent_s(inv4_enterprise_satisfied);
            result.setZjjinv4_ent_a(inv4_enterprise_dissatisfied);

            result.setZjjinv3_per_sss(inv3_personal_verygood);
            result.setZjjinv3_per_ss(inv3_personal_good);
            result.setZjjinv3_per_s(inv3_personal_satisfied);
            result.setZjjinv3_per_a(inv3_personal_dissatisfied);
            result.setZjjinv3_ent_sss(inv3_enterprise_verygood);
            result.setZjjinv3_ent_ss(inv3_enterprise_good);
            result.setZjjinv3_ent_s(inv3_enterprise_satisfied);
            result.setZjjinv3_ent_a(inv3_enterprise_dissatisfied);

            result.setZjjinv2_per_sss(inv2_personal_verygood);
            result.setZjjinv2_per_ss(inv2_personal_good);
            result.setZjjinv2_per_s(inv2_personal_satisfied);
            result.setZjjinv2_per_a(inv2_personal_dissatisfied);
            result.setZjjinv2_ent_sss(inv2_enterprise_verygood);
            result.setZjjinv2_ent_ss(inv2_enterprise_good);
            result.setZjjinv2_ent_s(inv2_enterprise_satisfied);
            result.setZjjinv2_ent_a(inv2_enterprise_dissatisfied);

            result.setZjjinv1_per_sss(inv1_personal_verygood);
            result.setZjjinv1_per_ss(inv1_personal_good);
            result.setZjjinv1_per_s(inv1_personal_satisfied);
            result.setZjjinv1_per_a(inv1_personal_dissatisfied);
            result.setZjjinv1_ent_sss(inv1_enterprise_verygood);
            result.setZjjinv1_ent_ss(inv1_enterprise_good);
            result.setZjjinv1_ent_s(inv1_enterprise_satisfied);
            result.setZjjinv1_ent_a(inv1_enterprise_dissatisfied);

            result.setZjjall_sss(all_verygood);
            result.setZjjall_ss(all_good);
            result.setZjjall_s(all_satisfied);
            result.setZjjall_a(all_dissatisfied);
            JSONObject jso = JSONObject.fromObject(result);
            return jso.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
