package springboot.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springboot.entity.InsertEntity;
import springboot.entity.ResultEntity;
import springboot.mapper.InvMapper;

/**
 * @ClassnameCzjController
 * @Description TODO
 * @Date 2022/1/2 10:56
 * @Created by GS
 **/
@RestController
@RequestMapping("CzjController")
public class CzjController {
    @Autowired
    InvMapper invMapper;
    //财政局数据查询
    @RequestMapping("/result")
    public String getCzjAll(){
        try {
            ResultEntity result = new ResultEntity();
            QueryWrapper<InsertEntity> queryWrapper = new QueryWrapper<>();
            long all_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "24").eq("all_inv", "非常满意"));
            queryWrapper.clear();
            long all_good = invMapper.selectCount(queryWrapper.eq("unit_id", "24").eq("all_inv", "比较满意"));
            queryWrapper.clear();
            long all_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "24").eq("all_inv", "基本满意"));
            queryWrapper.clear();
            long all_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "24").eq("all_inv", "不满意"));
            queryWrapper.clear();

            long inv1_enterprise_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "24").eq("obj", "企业").eq("inv1", "非常满意"));
            queryWrapper.clear();
            long inv1_enterprise_good = invMapper.selectCount(queryWrapper.eq("unit_id", "24").eq("obj", "企业").eq("inv1", "比较满意"));
            queryWrapper.clear();
            long inv1_enterprise_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "24").eq("obj", "企业").eq("inv1", "基本满意"));
            queryWrapper.clear();
            long inv1_enterprise_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "24").eq("obj", "企业").eq("inv1", "不满意"));
            queryWrapper.clear();


            long inv2_enterprise_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "24").eq("obj", "企业").eq("inv2", "非常满意"));
            queryWrapper.clear();
            long inv2_enterprise_good = invMapper.selectCount(queryWrapper.eq("unit_id", "24").eq("obj", "企业").eq("inv2", "比较满意"));
            queryWrapper.clear();
            long inv2_enterprise_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "24").eq("obj", "企业").eq("inv2", "基本满意"));
            queryWrapper.clear();
            long inv2_enterprise_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "24").eq("obj", "企业").eq("inv2", "不满意"));
            queryWrapper.clear();


            long inv3_enterprise_verygood = invMapper.selectCount(queryWrapper.eq("unit_id", "24").eq("obj", "企业").eq("inv3", "非常满意"));
            queryWrapper.clear();
            long inv3_enterprise_good = invMapper.selectCount(queryWrapper.eq("unit_id", "24").eq("obj", "企业").eq("inv3", "比较满意"));
            queryWrapper.clear();
            long inv3_enterprise_satisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "24").eq("obj", "企业").eq("inv3", "基本满意"));
            queryWrapper.clear();
            long inv3_enterprise_dissatisfied = invMapper.selectCount(queryWrapper.eq("unit_id", "24").eq("obj", "企业").eq("inv3", "不满意"));
            queryWrapper.clear();

            result.setZjjinv3_ent_sss(inv3_enterprise_verygood);
            result.setZjjinv3_ent_ss(inv3_enterprise_good);
            result.setZjjinv3_ent_s(inv3_enterprise_satisfied);
            result.setZjjinv3_ent_a(inv3_enterprise_dissatisfied);


            result.setZjjinv2_ent_sss(inv2_enterprise_verygood);
            result.setZjjinv2_ent_ss(inv2_enterprise_good);
            result.setZjjinv2_ent_s(inv2_enterprise_satisfied);
            result.setZjjinv2_ent_a(inv2_enterprise_dissatisfied);


            result.setZjjinv1_ent_sss(inv1_enterprise_verygood);
            result.setZjjinv1_ent_ss(inv1_enterprise_good);
            result.setZjjinv1_ent_s(inv1_enterprise_satisfied);
            result.setZjjinv1_ent_a(inv1_enterprise_dissatisfied);

            result.setZjjall_sss(all_verygood);
            result.setZjjall_ss(all_good);
            result.setZjjall_s(all_satisfied);
            result.setZjjall_a(all_dissatisfied);
            JSONObject jso = JSONObject.fromObject(result);
            return jso.toString();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }
}
