package springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springboot.entity.XcInsertEntity;
import springboot.mapper.XcInvMapper;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * xw ClassnameXcController
 * 2Description TODO
 * @Date 2021/12/25 12:04
 * @Created by GS
 **/
@RestController
@RequestMapping("XcInvController")
public class XcInvController {
    @Autowired
    XcInvMapper xcInvMapper;

    //插入数据
    @RequestMapping("/doSuccess")
    public int xcDoSuc(HttpServletRequest req) {
        XcInsertEntity inv = new XcInsertEntity();
        Date da = new Date();
        java.sql.Date date = new java.sql.Date(da.getTime());
        int unid = Integer.parseInt(req.getParameter("unit_id"));
        String allxc = req.getParameter("all_inv");
        String objxc = req.getParameter("obj");
        String invxc1 = req.getParameter("inv1");
        String invxc2 = req.getParameter("inv2");
        String invxc3 = req.getParameter("inv3");
        String invxc4 = req.getParameter("inv4");
        String invxc5 = req.getParameter("inv5");
        String invxc6 = req.getParameter("inv6");
        String invxc7 = req.getParameter("inv7");
        String invxc8 = req.getParameter("inv8");
        String invxc9 = req.getParameter("inv9");
        String invxc10 = req.getParameter("inv10");
        String invxc11 = req.getParameter("inv11");
        String invxc12 = req.getParameter("inv12");
        String invxc13 = req.getParameter("inv13");
        String invxc14 = req.getParameter("inv14");
        String invxc15 = req.getParameter("inv15");
        String invxc16 = req.getParameter("inv16");
        String invxc17 = req.getParameter("inv17");
        String invxc18 = req.getParameter("inv18");
        String invxc19 = req.getParameter("inv19");
        String invxc20 = req.getParameter("inv20");
        String invxc22 = req.getParameter("inv22");
        String xcopi = req.getParameter("opi");
        inv.setUnid(unid);
        inv.setAllxc(allxc);
        inv.setObjxc(objxc);
        inv.setInvxc1(invxc1);
        inv.setInvxc2(invxc2);
        inv.setInvxc3(invxc3);
        inv.setInvxc4(invxc4);
        inv.setInvxc5(invxc5);
        inv.setInvxc6(invxc6);
        inv.setInvxc7(invxc7);
        inv.setInvxc8(invxc8);
        inv.setInvxc9(invxc9);
        inv.setInvxc10(invxc10);
        inv.setInvxc11(invxc11);
        inv.setInvxc12(invxc12);
        inv.setInvxc13(invxc13);
        inv.setInvxc14(invxc14);
        inv.setInvxc15(invxc15);
        inv.setInvxc16(invxc16);
        inv.setInvxc17(invxc17);
        inv.setInvxc18(invxc18);
        inv.setInvxc19(invxc19);
        inv.setInvxc20(invxc20);
        inv.setInvxc21(date);
        inv.setInvxc22(invxc22);
        inv.setXcopi(xcopi);
        int insert = xcInvMapper.insert(inv);
        return insert;
    }
}























