package springboot.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springboot.entity.InsertEntity;
import springboot.mapper.InvMapper;
import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Classname InvController
 * @Description TODO
 * @Date 2021/12/2 11:40
 * @Created by GS
 */
@RestController
@RequestMapping("InvController")
public class InvController {
    private static Logger logger = LoggerFactory.getLogger(AtestController.class);
    private static final String IP_UTILS_FLAG = ",";
    private static final String UNKNOWN = "unknown";
    private static final String LOCALHOST_IP = "0:0:0:0:0:0:0:1";
    private static final String LOCALHOST_IP1 = "127.0.0.1";
    @Autowired
    InvMapper invMapper;
    //插入数据
    @RequestMapping("/doSuccess")
    public int doSuccess(HttpServletRequest req) {
        //根据对方提交数据时抓取对方的请求头中的ip
        String ip = null;
        try{//以下两个获取在k8s中，将真实的客户端IP，放到了x-Original-Forwarded-For。
            // 而将WAF的回源地址放到了 x-Forwarded-For了。
            ip = req.getHeader("X-Original-Forwarded-For");
            if (StringUtils.isEmpty(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = req.getHeader("X-Forwarded-For");
            }
            //获取nginx等代理的ip
            if (StringUtils.isEmpty(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = req.getHeader("x-forwarded-for");
            }
            if (StringUtils.isEmpty(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = req.getHeader("Proxy-Client-IP");
            }
            if (StringUtils.isEmpty(ip) || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = req.getHeader("WL-Proxy-Client-IP");
            }
            if (StringUtils.isEmpty(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = req.getHeader("HTTP_X_FORWARDED_FOR");
            }
            //兼容k8s集群获取ip
            if (StringUtils.isEmpty(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = req.getRemoteAddr();
                if (LOCALHOST_IP1.equalsIgnoreCase(ip) || LOCALHOST_IP.equalsIgnoreCase(ip)) {
                    //根据网卡取本机配置的IP
                    InetAddress iNet = null;
                    try {
                        iNet = InetAddress.getLocalHost();
                    } catch (UnknownHostException e) {
                        logger.error("getClientIp error: {}", e);
                    }
                    ip = iNet.getHostAddress();

                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        //使用代理，则获取第一个IP地址
        if (!StringUtils.isEmpty(ip) && ip.indexOf(IP_UTILS_FLAG) > 0) {
            ip = ip.substring(0, ip.indexOf(IP_UTILS_FLAG));
        }

        System.out.println(ip);


        InsertEntity inv = new InsertEntity();
        Date d = new Date();
        java.sql.Date date = new java.sql.Date(d.getTime());
        int unit_id = Integer.parseInt(req.getParameter("unit_id"));
        String all_inv = req.getParameter("all_inv");
        String obj = req.getParameter("obj");
        String inv1 = req.getParameter("inv1");
        String inv2 = req.getParameter("inv2");
        String inv3 = req.getParameter("inv3");
        String inv4 = req.getParameter("inv4");
        String inv5 = req.getParameter("inv5");
        String inv6 = req.getParameter("inv6");
        String inv7 = req.getParameter("inv7");
        String inv8 = req.getParameter("inv8");
        String inv9 = req.getParameter("inv9");
        String inv10 = req.getParameter("inv10");
        String inv11 = req.getParameter("inv11");
        String inv12 = req.getParameter("inv12");
        String opi = req.getParameter("opi");
        inv.setUnit_id(unit_id);
        inv.setAll_inv(all_inv);
        inv.setObj(obj);
        inv.setInv1(inv1);
        inv.setInv2(inv2);
        inv.setInv3(inv3);
        inv.setInv4(inv4);
        inv.setInv5(inv5);
        inv.setInv6(inv6);
        inv.setInv7(inv7);
        inv.setInv8(inv8);
        inv.setInv9(inv9);
        inv.setInv10(inv10);
        inv.setInv11(inv11);
        inv.setInv12(inv12);
        inv.setInv13(date);
        inv.setOpi(opi);
        //通过mapper继承baseMapper调用insert方法返回插入数据条数
        int insert = invMapper.insert(inv);
        //System.out.println("成功插入"+insert+"条数据");
        return insert;
    };



}
