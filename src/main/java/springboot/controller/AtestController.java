package springboot.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import springboot.mapper.InvMapper;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;


/**
 * @Classname InvController
 * @Description TODO
 * @Date 2021/12/2 11:40
 * @Created by GS
 */
@RestController
@RequestMapping("TestController")
public class AtestController {
    private static Logger logger = LoggerFactory.getLogger(AtestController.class);
    private static final String IP_UTILS_FLAG = ",";
    private static final String UNKNOWN = "unknown";
    private static final String LOCALHOST_IP = "0:0:0:0:0:0:0:1";
    private static final String LOCALHOST_IP1 = "127.0.0.1";
    @Autowired
    InvMapper invMapper;

    /**
     * 获取IP地址
     * <p>
     * 使用Nginx等反向代理软件， 则不能通过request.getRemoteAddr()获取IP地址
     * 如果使用了多级反向代理的话，X-Forwarded-For的值并不止一个，
     * 而是一串IP地址，X-Forwarded-For中第一个非unknown的有效IP字符串，则为真实IP地址
     *   server {
     *         listen       8080;
     *         server_name  yspjb.hld.gov.cn;
     *
     *         location / {
     *
     *           proxy_pass http://localhost:8081;
     *           NGINX配置用户请求的转发头不然 只会用nginx的本地服务请求头
     * 	        proxy_set_header        X-Real-IP       $remote_addr;
     *           proxy_set_header        Host            $host;
     *           proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
     *           proxy_pass_request_headers              on;
     *         }
     *     }
     */
    @RequestMapping("/doSuccess")
    public String doSuccess(HttpServletRequest req) {
        //根据对方提交数据时抓取对方的请求头中的ip
        String ip = null;
        try{//以下两个获取在k8s中，将真实的客户端IP，放到了x-Original-Forwarded-For。
            // 而将WAF的回源地址放到了 x-Forwarded-For了。
            ip = req.getHeader("X-Original-Forwarded-For");
            if (StringUtils.isEmpty(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = req.getHeader("X-Forwarded-For");
            }
            //获取nginx等代理的ip
            if (StringUtils.isEmpty(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = req.getHeader("x-forwarded-for");
            }
            if (StringUtils.isEmpty(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = req.getHeader("Proxy-Client-IP");
            }
            if (StringUtils.isEmpty(ip) || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = req.getHeader("WL-Proxy-Client-IP");
            }
            if (StringUtils.isEmpty(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = req.getHeader("HTTP_X_FORWARDED_FOR");
            }
            //兼容k8s集群获取ip
            if (StringUtils.isEmpty(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = req.getRemoteAddr();
                if (LOCALHOST_IP1.equalsIgnoreCase(ip) || LOCALHOST_IP.equalsIgnoreCase(ip)) {
                    //根据网卡取本机配置的IP
                    InetAddress iNet = null;
                    try {
                        iNet = InetAddress.getLocalHost();
                    } catch (UnknownHostException e) {
                        logger.error("getClientIp error: {}", e);
                    }
                    ip = iNet.getHostAddress();

                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        //使用代理，则获取第一个IP地址
        if (!StringUtils.isEmpty(ip) && ip.indexOf(IP_UTILS_FLAG) > 0) {
            ip = ip.substring(0, ip.indexOf(IP_UTILS_FLAG));
        }


        System.out.println(ip);

        return ip;
    };



}
