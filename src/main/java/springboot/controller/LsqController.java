package springboot.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springboot.entity.XcInsertEntity;
import springboot.entity.XcResultEntity;
import springboot.mapper.XcInvMapper;

/**
 * @ClassnameLgqController
 * @Description TODO
 * @Date 2022/1/9 18:41
 * @Created by GS
 **/
@RestController
@RequestMapping("LsqController")
public class LsqController {
    @Autowired
    XcInvMapper xcInvMapper;

    @RequestMapping("/result")
    public String getGajAll(){
        try {
            XcResultEntity xcResult = new XcResultEntity();
            QueryWrapper<XcInsertEntity> queryWrapper = new QueryWrapper<>();
            long all_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("allxc", "非常满意"));
            queryWrapper.clear();
            long all_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("allxc", "比较满意"));
            queryWrapper.clear();
            long all_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("allxc", "基本满意"));
            queryWrapper.clear();
            long all_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("allxc", "不满意"));
            queryWrapper.clear();

            long inv1_personal_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc1", "非常满意"));
            queryWrapper.clear();
            long inv1_personal_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc1", "比较满意"));
            queryWrapper.clear();
            long inv1_personal_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc1", "基本满意"));
            queryWrapper.clear();
            long inv1_personal_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc1", "不满意"));
            queryWrapper.clear();
            long inv1_enterprise_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc1", "非常满意"));
            queryWrapper.clear();
            long inv1_enterprise_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc1", "比较满意"));
            queryWrapper.clear();
            long inv1_enterprise_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc1", "基本满意"));
            queryWrapper.clear();
            long inv1_enterprise_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc1", "不满意"));
            queryWrapper.clear();

            long inv2_personal_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc2", "非常满意"));
            queryWrapper.clear();
            long inv2_personal_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc2", "比较满意"));
            queryWrapper.clear();
            long inv2_personal_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc2", "基本满意"));
            queryWrapper.clear();
            long inv2_personal_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc2", "不满意"));
            queryWrapper.clear();
            long inv2_enterprise_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc2", "非常满意"));
            queryWrapper.clear();
            long inv2_enterprise_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc2", "比较满意"));
            queryWrapper.clear();
            long inv2_enterprise_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc2", "基本满意"));
            queryWrapper.clear();
            long inv2_enterprise_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc2", "不满意"));
            queryWrapper.clear();

            long inv3_personal_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc3", "非常满意"));
            queryWrapper.clear();
            long inv3_personal_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc3", "比较满意"));
            queryWrapper.clear();
            long inv3_personal_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc3", "基本满意"));
            queryWrapper.clear();
            long inv3_personal_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc3", "不满意"));
            queryWrapper.clear();
            long inv3_enterprise_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc3", "非常满意"));
            queryWrapper.clear();
            long inv3_enterprise_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc3", "比较满意"));
            queryWrapper.clear();
            long inv3_enterprise_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc3", "基本满意"));
            queryWrapper.clear();
            long inv3_enterprise_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc3", "不满意"));
            queryWrapper.clear();

            long inv4_personal_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc4", "非常满意"));
            queryWrapper.clear();
            long inv4_personal_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc4", "比较满意"));
            queryWrapper.clear();
            long inv4_personal_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc4", "基本满意"));
            queryWrapper.clear();
            long inv4_personal_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc4", "不满意"));
            queryWrapper.clear();
            long inv4_enterprise_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc4", "非常满意"));
            queryWrapper.clear();
            long inv4_enterprise_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc4", "比较满意"));
            queryWrapper.clear();
            long inv4_enterprise_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc4", "基本满意"));
            queryWrapper.clear();
            long inv4_enterprise_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc4", "不满意"));
            queryWrapper.clear();

            long inv5_personal_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc5", "非常满意"));
            queryWrapper.clear();
            long inv5_personal_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc5", "比较满意"));
            queryWrapper.clear();
            long inv5_personal_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc5", "基本满意"));
            queryWrapper.clear();
            long inv5_personal_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc5", "不满意"));
            queryWrapper.clear();
            long inv5_enterprise_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc5", "非常满意"));
            queryWrapper.clear();
            long inv5_enterprise_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc5", "比较满意"));
            queryWrapper.clear();
            long inv5_enterprise_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc5", "基本满意"));
            queryWrapper.clear();
            long inv5_enterprise_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc5", "不满意"));
            queryWrapper.clear();

            long inv6_personal_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc6", "非常满意"));
            queryWrapper.clear();
            long inv6_personal_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc6", "比较满意"));
            queryWrapper.clear();
            long inv6_personal_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc6", "基本满意"));
            queryWrapper.clear();
            long inv6_personal_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc6", "不满意"));
            queryWrapper.clear();
            long inv6_enterprise_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc6", "非常满意"));
            queryWrapper.clear();
            long inv6_enterprise_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc6", "比较满意"));
            queryWrapper.clear();
            long inv6_enterprise_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc6", "基本满意"));
            queryWrapper.clear();
            long inv6_enterprise_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc6", "不满意"));

            queryWrapper.clear();
            long inv7_personal_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc7", "非常满意"));
            queryWrapper.clear();
            long inv7_personal_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc7", "比较满意"));
            queryWrapper.clear();
            long inv7_personal_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc7", "基本满意"));
            queryWrapper.clear();
            long inv7_personal_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc7", "不满意"));
            queryWrapper.clear();
            long inv7_enterprise_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc7", "非常满意"));
            queryWrapper.clear();
            long inv7_enterprise_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc7", "比较满意"));
            queryWrapper.clear();
            long inv7_enterprise_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc7", "基本满意"));
            queryWrapper.clear();
            long inv7_enterprise_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc7", "不满意"));
            queryWrapper.clear();

            long inv8_personal_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc8", "非常满意"));
            queryWrapper.clear();
            long inv8_personal_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc8", "比较满意"));
            queryWrapper.clear();
            long inv8_personal_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc8", "基本满意"));
            queryWrapper.clear();
            long inv8_personal_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc8", "不满意"));
            queryWrapper.clear();
            long inv8_enterprise_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc8", "非常满意"));
            queryWrapper.clear();
            long inv8_enterprise_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc8", "比较满意"));
            queryWrapper.clear();
            long inv8_enterprise_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc8", "基本满意"));
            queryWrapper.clear();
            long inv8_enterprise_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc8", "不满意"));
            queryWrapper.clear();

            long inv9_personal_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc9", "非常满意"));
            queryWrapper.clear();
            long inv9_personal_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc9", "比较满意"));
            queryWrapper.clear();
            long inv9_personal_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc9", "基本满意"));
            queryWrapper.clear();
            long inv9_personal_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc9", "不满意"));
            queryWrapper.clear();
            long inv9_enterprise_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc9", "非常满意"));
            queryWrapper.clear();
            long inv9_enterprise_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc9", "比较满意"));
            queryWrapper.clear();
            long inv9_enterprise_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc9", "基本满意"));
            queryWrapper.clear();
            long inv9_enterprise_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc9", "不满意"));
            queryWrapper.clear();

            long inv10_personal_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc10", "非常满意"));
            queryWrapper.clear();
            long inv10_personal_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc10", "比较满意"));
            queryWrapper.clear();
            long inv10_personal_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc10", "基本满意"));
            queryWrapper.clear();
            long inv10_personal_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc10", "不满意"));
            queryWrapper.clear();
            long inv10_enterprise_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc10", "非常满意"));
            queryWrapper.clear();
            long inv10_enterprise_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc10", "比较满意"));
            queryWrapper.clear();
            long inv10_enterprise_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc10", "基本满意"));
            queryWrapper.clear();
            long inv10_enterprise_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc10", "不满意"));
            queryWrapper.clear();

            long inv11_personal_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc11", "非常满意"));
            queryWrapper.clear();
            long inv11_personal_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc11", "比较满意"));
            queryWrapper.clear();
            long inv11_personal_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc11", "基本满意"));
            queryWrapper.clear();
            long inv11_personal_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc11", "不满意"));
            queryWrapper.clear();
            long inv11_enterprise_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc11", "非常满意"));
            queryWrapper.clear();
            long inv11_enterprise_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc11", "比较满意"));
            queryWrapper.clear();
            long inv11_enterprise_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc11", "基本满意"));
            queryWrapper.clear();
            long inv11_enterprise_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc11", "不满意"));
            queryWrapper.clear();

            long inv12_personal_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc12", "非常满意"));
            queryWrapper.clear();
            long inv12_personal_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc12", "比较满意"));
            queryWrapper.clear();
            long inv12_personal_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc12", "基本满意"));
            queryWrapper.clear();
            long inv12_personal_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc12", "不满意"));
            queryWrapper.clear();
            long inv12_enterprise_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc12", "非常满意"));
            queryWrapper.clear();
            long inv12_enterprise_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc12", "比较满意"));
            queryWrapper.clear();
            long inv12_enterprise_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc12", "基本满意"));
            queryWrapper.clear();
            long inv12_enterprise_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc12", "不满意"));
            queryWrapper.clear();

            long inv13_personal_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc13", "非常满意"));
            queryWrapper.clear();
            long inv13_personal_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc13", "比较满意"));
            queryWrapper.clear();
            long inv13_personal_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc13", "基本满意"));
            queryWrapper.clear();
            long inv13_personal_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc13", "不满意"));
            queryWrapper.clear();
            long inv13_enterprise_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc13", "非常满意"));
            queryWrapper.clear();
            long inv13_enterprise_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc13", "比较满意"));
            queryWrapper.clear();
            long inv13_enterprise_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc13", "基本满意"));
            queryWrapper.clear();
            long inv13_enterprise_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc13", "不满意"));
            queryWrapper.clear();


            long inv14_personal_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc14", "非常满意"));
            queryWrapper.clear();
            long inv14_personal_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc14", "比较满意"));
            queryWrapper.clear();
            long inv14_personal_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc14", "基本满意"));
            queryWrapper.clear();
            long inv14_personal_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc14", "不满意"));
            queryWrapper.clear();
            long inv14_enterprise_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc14", "非常满意"));
            queryWrapper.clear();
            long inv14_enterprise_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc14", "比较满意"));
            queryWrapper.clear();
            long inv14_enterprise_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc14", "基本满意"));
            queryWrapper.clear();
            long inv14_enterprise_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc14", "不满意"));
            queryWrapper.clear();

            long inv15_personal_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc15", "非常满意"));
            queryWrapper.clear();
            long inv15_personal_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc15", "比较满意"));
            queryWrapper.clear();
            long inv15_personal_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc15", "基本满意"));
            queryWrapper.clear();
            long inv15_personal_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc15", "不满意"));
            queryWrapper.clear();
            long inv15_enterprise_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc15", "非常满意"));
            queryWrapper.clear();
            long inv15_enterprise_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc15", "比较满意"));
            queryWrapper.clear();
            long inv15_enterprise_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc15", "基本满意"));
            queryWrapper.clear();
            long inv15_enterprise_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc15", "不满意"));
            queryWrapper.clear();

            long inv16_personal_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc16", "非常满意"));
            queryWrapper.clear();
            long inv16_personal_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc16", "比较满意"));
            queryWrapper.clear();
            long inv16_personal_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc16", "基本满意"));
            queryWrapper.clear();
            long inv16_personal_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc16", "不满意"));
            queryWrapper.clear();
            long inv16_enterprise_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc16", "非常满意"));
            queryWrapper.clear();
            long inv16_enterprise_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc16", "比较满意"));
            queryWrapper.clear();
            long inv16_enterprise_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc16", "基本满意"));
            queryWrapper.clear();
            long inv16_enterprise_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc16", "不满意"));
            queryWrapper.clear();

            long inv17_personal_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc17", "非常满意"));
            queryWrapper.clear();
            long inv17_personal_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc17", "比较满意"));
            queryWrapper.clear();
            long inv17_personal_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc17", "基本满意"));
            queryWrapper.clear();
            long inv17_personal_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc17", "不满意"));
            queryWrapper.clear();
            long inv17_enterprise_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc17", "非常满意"));
            queryWrapper.clear();
            long inv17_enterprise_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc17", "比较满意"));
            queryWrapper.clear();
            long inv17_enterprise_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc17", "基本满意"));
            queryWrapper.clear();
            long inv17_enterprise_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc17", "不满意"));
            queryWrapper.clear();

            long inv18_personal_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc18", "非常满意"));
            queryWrapper.clear();
            long inv18_personal_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc18", "比较满意"));
            queryWrapper.clear();
            long inv18_personal_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc18", "基本满意"));
            queryWrapper.clear();
            long inv18_personal_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc18", "不满意"));
            queryWrapper.clear();
            long inv18_enterprise_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc18", "非常满意"));
            queryWrapper.clear();
            long inv18_enterprise_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc18", "比较满意"));
            queryWrapper.clear();
            long inv18_enterprise_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc18", "基本满意"));
            queryWrapper.clear();
            long inv18_enterprise_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc18", "不满意"));
            queryWrapper.clear();

            long inv19_personal_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc19", "非常满意"));
            queryWrapper.clear();
            long inv19_personal_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc19", "比较满意"));
            queryWrapper.clear();
            long inv19_personal_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc19", "基本满意"));
            queryWrapper.clear();
            long inv19_personal_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc19", "不满意"));
            queryWrapper.clear();
            long inv19_enterprise_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc19", "非常满意"));
            queryWrapper.clear();
            long inv19_enterprise_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc19", "比较满意"));
            queryWrapper.clear();
            long inv19_enterprise_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc19", "基本满意"));
            queryWrapper.clear();
            long inv19_enterprise_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc19", "不满意"));
            queryWrapper.clear();

            long inv20_personal_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc20", "非常满意"));
            queryWrapper.clear();
            long inv20_personal_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc20", "比较满意"));
            queryWrapper.clear();
            long inv20_personal_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc20", "基本满意"));
            queryWrapper.clear();
            long inv20_personal_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc20", "不满意"));
            queryWrapper.clear();
            long inv20_enterprise_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc20", "非常满意"));
            queryWrapper.clear();
            long inv20_enterprise_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc20", "比较满意"));
            queryWrapper.clear();
            long inv20_enterprise_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc20", "基本满意"));
            queryWrapper.clear();
            long inv20_enterprise_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc20", "不满意"));
            queryWrapper.clear();

            long inv22_personal_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc22", "非常满意"));
            queryWrapper.clear();
            long inv22_personal_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc22", "比较满意"));
            queryWrapper.clear();
            long inv22_personal_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc22", "基本满意"));
            queryWrapper.clear();
            long inv22_personal_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "个人").eq("invxc22", "不满意"));
            queryWrapper.clear();
            long inv22_enterprise_verygood = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc22", "非常满意"));
            queryWrapper.clear();
            long inv22_enterprise_good = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc22", "比较满意"));
            queryWrapper.clear();
            long inv22_enterprise_satisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc22", "基本满意"));
            queryWrapper.clear();
            long inv22_enterprise_dissatisfied = xcInvMapper.selectCount(queryWrapper.eq("unid", "36").eq("objxc", "企业").eq("invxc22", "不满意"));
            queryWrapper.clear();


            xcResult.setAllxc_sss(all_verygood);
            xcResult.setAllxc_ss(all_good);
            xcResult.setAllxc_s(all_satisfied);
            xcResult.setAllxc_a(all_dissatisfied);
            xcResult.setInvxc1_ent_sss(inv1_enterprise_verygood);
            xcResult.setInvxc1_ent_ss(inv1_enterprise_good);
            xcResult.setInvxc1_ent_s(inv1_enterprise_satisfied);
            xcResult.setInvxc1_ent_a(inv1_enterprise_dissatisfied);
            xcResult.setInvxc1_per_sss(inv1_personal_verygood);
            xcResult.setInvxc1_per_ss(inv1_personal_good);
            xcResult.setInvxc1_per_s(inv1_personal_satisfied);
            xcResult.setInvxc1_per_a(inv1_personal_dissatisfied);

            xcResult.setInvxc2_ent_sss(inv2_enterprise_verygood);
            xcResult.setInvxc2_ent_ss(inv2_enterprise_good);
            xcResult.setInvxc2_ent_s(inv2_enterprise_satisfied);
            xcResult.setInvxc2_ent_a(inv2_enterprise_dissatisfied);
            xcResult.setInvxc2_per_sss(inv2_personal_verygood);
            xcResult.setInvxc2_per_ss(inv2_personal_good);
            xcResult.setInvxc2_per_s(inv2_personal_satisfied);
            xcResult.setInvxc2_per_a(inv2_personal_dissatisfied);

            xcResult.setInvxc3_ent_sss(inv3_enterprise_verygood);
            xcResult.setInvxc3_ent_ss(inv3_enterprise_good);
            xcResult.setInvxc3_ent_s(inv3_enterprise_satisfied);
            xcResult.setInvxc3_ent_a(inv3_enterprise_dissatisfied);
            xcResult.setInvxc3_per_sss(inv3_personal_verygood);
            xcResult.setInvxc3_per_ss(inv3_personal_good);
            xcResult.setInvxc3_per_s(inv3_personal_satisfied);
            xcResult.setInvxc3_per_a(inv3_personal_dissatisfied);

            xcResult.setInvxc4_ent_sss(inv4_enterprise_verygood);
            xcResult.setInvxc4_ent_ss(inv4_enterprise_good);
            xcResult.setInvxc4_ent_s(inv4_enterprise_satisfied);
            xcResult.setInvxc4_ent_a(inv4_enterprise_dissatisfied);
            xcResult.setInvxc4_per_sss(inv4_personal_verygood);
            xcResult.setInvxc4_per_ss(inv4_personal_good);
            xcResult.setInvxc4_per_s(inv4_personal_satisfied);
            xcResult.setInvxc4_per_a(inv4_personal_dissatisfied);

            xcResult.setInvxc5_ent_sss(inv5_enterprise_verygood);
            xcResult.setInvxc5_ent_ss(inv5_enterprise_good);
            xcResult.setInvxc5_ent_s(inv5_enterprise_satisfied);
            xcResult.setInvxc5_ent_a(inv5_enterprise_dissatisfied);
            xcResult.setInvxc5_per_sss(inv5_personal_verygood);
            xcResult.setInvxc5_per_ss(inv5_personal_good);
            xcResult.setInvxc5_per_s(inv5_personal_satisfied);
            xcResult.setInvxc5_per_a(inv5_personal_dissatisfied);

            xcResult.setInvxc6_ent_sss(inv6_enterprise_verygood);
            xcResult.setInvxc6_ent_ss(inv6_enterprise_good);
            xcResult.setInvxc6_ent_s(inv6_enterprise_satisfied);
            xcResult.setInvxc6_ent_a(inv6_enterprise_dissatisfied);
            xcResult.setInvxc6_per_sss(inv6_personal_verygood);
            xcResult.setInvxc6_per_ss(inv6_personal_good);
            xcResult.setInvxc6_per_s(inv6_personal_satisfied);
            xcResult.setInvxc6_per_a(inv6_personal_dissatisfied);

            xcResult.setInvxc7_ent_sss(inv7_enterprise_verygood);
            xcResult.setInvxc7_ent_ss(inv7_enterprise_good);
            xcResult.setInvxc7_ent_s(inv7_enterprise_satisfied);
            xcResult.setInvxc7_ent_a(inv7_enterprise_dissatisfied);
            xcResult.setInvxc7_per_sss(inv7_personal_verygood);
            xcResult.setInvxc7_per_ss(inv7_personal_good);
            xcResult.setInvxc7_per_s(inv7_personal_satisfied);
            xcResult.setInvxc7_per_a(inv7_personal_dissatisfied);

            xcResult.setInvxc8_ent_sss(inv8_enterprise_verygood);
            xcResult.setInvxc8_ent_ss(inv8_enterprise_good);
            xcResult.setInvxc8_ent_s(inv8_enterprise_satisfied);
            xcResult.setInvxc8_ent_a(inv8_enterprise_dissatisfied);
            xcResult.setInvxc8_per_sss(inv8_personal_verygood);
            xcResult.setInvxc8_per_ss(inv8_personal_good);
            xcResult.setInvxc8_per_s(inv8_personal_satisfied);
            xcResult.setInvxc8_per_a(inv8_personal_dissatisfied);

            xcResult.setInvxc9_ent_sss(inv9_enterprise_verygood);
            xcResult.setInvxc9_ent_ss(inv9_enterprise_good);
            xcResult.setInvxc9_ent_s(inv9_enterprise_satisfied);
            xcResult.setInvxc9_ent_a(inv9_enterprise_dissatisfied);
            xcResult.setInvxc9_per_sss(inv9_personal_verygood);
            xcResult.setInvxc9_per_ss(inv9_personal_good);
            xcResult.setInvxc9_per_s(inv9_personal_satisfied);
            xcResult.setInvxc9_per_a(inv9_personal_dissatisfied);

            xcResult.setInvxc10_ent_sss(inv10_enterprise_verygood);
            xcResult.setInvxc10_ent_ss(inv10_enterprise_good);
            xcResult.setInvxc10_ent_s(inv10_enterprise_satisfied);
            xcResult.setInvxc10_ent_a(inv10_enterprise_dissatisfied);
            xcResult.setInvxc10_per_sss(inv10_personal_verygood);
            xcResult.setInvxc10_per_ss(inv10_personal_good);
            xcResult.setInvxc10_per_s(inv10_personal_satisfied);
            xcResult.setInvxc10_per_a(inv10_personal_dissatisfied);

            xcResult.setInvxc11_ent_sss(inv11_enterprise_verygood);
            xcResult.setInvxc11_ent_ss(inv11_enterprise_good);
            xcResult.setInvxc11_ent_s(inv11_enterprise_satisfied);
            xcResult.setInvxc11_ent_a(inv11_enterprise_dissatisfied);
            xcResult.setInvxc11_per_sss(inv11_personal_verygood);
            xcResult.setInvxc11_per_ss(inv11_personal_good);
            xcResult.setInvxc11_per_s(inv11_personal_satisfied);
            xcResult.setInvxc11_per_a(inv11_personal_dissatisfied);

            xcResult.setInvxc12_ent_sss(inv12_enterprise_verygood);
            xcResult.setInvxc12_ent_ss(inv12_enterprise_good);
            xcResult.setInvxc12_ent_s(inv12_enterprise_satisfied);
            xcResult.setInvxc12_ent_a(inv12_enterprise_dissatisfied);
            xcResult.setInvxc12_per_sss(inv12_personal_verygood);
            xcResult.setInvxc12_per_ss(inv12_personal_good);
            xcResult.setInvxc12_per_s(inv12_personal_satisfied);
            xcResult.setInvxc12_per_a(inv12_personal_dissatisfied);

            xcResult.setInvxc13_ent_sss(inv13_enterprise_verygood);
            xcResult.setInvxc13_ent_ss(inv13_enterprise_good);
            xcResult.setInvxc13_ent_s(inv13_enterprise_satisfied);
            xcResult.setInvxc13_ent_a(inv13_enterprise_dissatisfied);
            xcResult.setInvxc13_per_sss(inv13_personal_verygood);
            xcResult.setInvxc13_per_ss(inv13_personal_good);
            xcResult.setInvxc13_per_s(inv13_personal_satisfied);
            xcResult.setInvxc13_per_a(inv13_personal_dissatisfied);

            xcResult.setInvxc14_ent_sss(inv14_enterprise_verygood);
            xcResult.setInvxc14_ent_ss(inv14_enterprise_good);
            xcResult.setInvxc14_ent_s(inv14_enterprise_satisfied);
            xcResult.setInvxc14_ent_a(inv14_enterprise_dissatisfied);
            xcResult.setInvxc14_per_sss(inv14_personal_verygood);
            xcResult.setInvxc14_per_ss(inv14_personal_good);
            xcResult.setInvxc14_per_s(inv14_personal_satisfied);
            xcResult.setInvxc14_per_a(inv14_personal_dissatisfied);

            xcResult.setInvxc15_ent_sss(inv15_enterprise_verygood);
            xcResult.setInvxc15_ent_ss(inv15_enterprise_good);
            xcResult.setInvxc15_ent_s(inv15_enterprise_satisfied);
            xcResult.setInvxc15_ent_a(inv15_enterprise_dissatisfied);
            xcResult.setInvxc15_per_sss(inv15_personal_verygood);
            xcResult.setInvxc15_per_ss(inv15_personal_good);
            xcResult.setInvxc15_per_s(inv15_personal_satisfied);
            xcResult.setInvxc15_per_a(inv15_personal_dissatisfied);

            xcResult.setInvxc16_ent_sss(inv16_enterprise_verygood);
            xcResult.setInvxc16_ent_ss(inv16_enterprise_good);
            xcResult.setInvxc16_ent_s(inv16_enterprise_satisfied);
            xcResult.setInvxc16_ent_a(inv16_enterprise_dissatisfied);
            xcResult.setInvxc16_per_sss(inv16_personal_verygood);
            xcResult.setInvxc16_per_ss(inv16_personal_good);
            xcResult.setInvxc16_per_s(inv16_personal_satisfied);
            xcResult.setInvxc16_per_a(inv16_personal_dissatisfied);

            xcResult.setInvxc17_ent_sss(inv17_enterprise_verygood);
            xcResult.setInvxc17_ent_ss(inv17_enterprise_good);
            xcResult.setInvxc17_ent_s(inv17_enterprise_satisfied);
            xcResult.setInvxc17_ent_a(inv17_enterprise_dissatisfied);
            xcResult.setInvxc17_per_sss(inv17_personal_verygood);
            xcResult.setInvxc17_per_ss(inv17_personal_good);
            xcResult.setInvxc17_per_s(inv17_personal_satisfied);
            xcResult.setInvxc17_per_a(inv17_personal_dissatisfied);

            xcResult.setInvxc18_ent_sss(inv18_enterprise_verygood);
            xcResult.setInvxc18_ent_ss(inv18_enterprise_good);
            xcResult.setInvxc18_ent_s(inv18_enterprise_satisfied);
            xcResult.setInvxc18_ent_a(inv18_enterprise_dissatisfied);
            xcResult.setInvxc18_per_sss(inv18_personal_verygood);
            xcResult.setInvxc18_per_ss(inv18_personal_good);
            xcResult.setInvxc18_per_s(inv18_personal_satisfied);
            xcResult.setInvxc18_per_a(inv18_personal_dissatisfied);

            xcResult.setInvxc19_ent_sss(inv19_enterprise_verygood);
            xcResult.setInvxc19_ent_ss(inv19_enterprise_good);
            xcResult.setInvxc19_ent_s(inv19_enterprise_satisfied);
            xcResult.setInvxc19_ent_a(inv19_enterprise_dissatisfied);
            xcResult.setInvxc19_per_sss(inv19_personal_verygood);
            xcResult.setInvxc19_per_ss(inv19_personal_good);
            xcResult.setInvxc19_per_s(inv19_personal_satisfied);
            xcResult.setInvxc19_per_a(inv19_personal_dissatisfied);

            xcResult.setInvxc20_ent_sss(inv20_enterprise_verygood);
            xcResult.setInvxc20_ent_ss(inv20_enterprise_good);
            xcResult.setInvxc20_ent_s(inv20_enterprise_satisfied);
            xcResult.setInvxc20_ent_a(inv20_enterprise_dissatisfied);
            xcResult.setInvxc20_per_sss(inv20_personal_verygood);
            xcResult.setInvxc20_per_ss(inv20_personal_good);
            xcResult.setInvxc20_per_s(inv20_personal_satisfied);
            xcResult.setInvxc20_per_a(inv20_personal_dissatisfied);

            xcResult.setInvxc22_ent_sss(inv22_enterprise_verygood);
            xcResult.setInvxc22_ent_ss(inv22_enterprise_good);
            xcResult.setInvxc22_ent_s(inv22_enterprise_satisfied);
            xcResult.setInvxc22_ent_a(inv22_enterprise_dissatisfied);
            xcResult.setInvxc22_per_sss(inv22_personal_verygood);
            xcResult.setInvxc22_per_ss(inv22_personal_good);
            xcResult.setInvxc22_per_s(inv22_personal_satisfied);
            xcResult.setInvxc22_per_a(inv22_personal_dissatisfied);


            JSONObject jso = JSONObject.fromObject(xcResult);
            return jso.toString();

        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }
}
